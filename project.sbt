name := "TestDKBigDataInfra"

version := "1.0.1"

scalaVersion := "2.12.3"

val sparkVersion = "2.4.0"

resolvers ++= Seq(
  ("apache-snapshots" at "http://repository.apache.org/snapshots/").withAllowInsecureProtocol(true)
)

libraryDependencies ++= Seq(
  "mysql" % "mysql-connector-java" % "5.1.+",
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" %% "spark-mllib" % sparkVersion,
  "org.apache.spark" %% "spark-streaming" % sparkVersion,
  "org.apache.spark" % "spark-streaming-flume_2.12" % "2.4.1",
  "com.datastax.spark" % "spark-cassandra-connector_2.12" % "2.4.2",
  "edu.stanford.nlp" % "stanford-corenlp" % "3.6.0",
  "edu.stanford.nlp" % "stanford-corenlp" % "3.6.0" classifier "models",
  "com.datastax.spark" %% "spark-cassandra-connector" % "2.4.3",
  "org.apache.spark" %% "spark-hive" % sparkVersion)
  
