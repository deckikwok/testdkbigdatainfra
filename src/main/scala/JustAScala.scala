import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.streaming.flume._
//import org.apache.spark.streaming.flume._//FlumeUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import java.io.File
import scala.annotation.tailrec
import com.datastax.spark.connector.cql._ //(Optional) Imports java driver helper functions
import com.datastax.spark.connector._
import org.apache.spark.sql.cassandra._

import scala.util.parsing.json._
//for scala >2.11, json lib deprecated.use:
//libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4"

//import edu.stanford.nlp.sentiment.SentimentUtils._

object JustAScala {

	//SET ALL OBJECT CONST
	val PI =3.14159
			def main(args: Array[String]): Unit = {

					//SET ALL DEF MAIN CONST
					val listInt = List(1,2,3,4)

							System.out.println("why not just java...")  

							//	testSpark(args);
							//							val circleAreaResult = circleArea(8)
							//							println("Circle area result is : ", circleAreaResult)
							//
							//							println("Circle Area using FILLED String radius is : " + circleAreaStr("8"))
							//							println("Circle Area using EMPTY String radius is : " + circleAreaStr(""))
							//
							//							println(pdtArrayElem(listInt))
							//							
							//							println(exponentfx(2, 3))
							//							
							//							println(tuplediff((4,9), (122,27)))

							val sparkConf = new SparkConf()
							.setAppName("FlumePushWordCount")
							.setMaster("local[2]")
							.set("spark.cassandra.connection.host", "localhost")
							//.set("spark.cassandra.auth.username", "testuser")            
							//.set("spark.cassandra.auth.password", "password")

							val sc = new SparkContext(sparkConf)
							val cc = CassandraConnector(sc.getConf)

							// SET UP DB
							cc.withSessionDo ( session => session.execute(
									"CREATE KEYSPACE IF NOT EXISTS dk_test_keyspace WITH replication = {'class':'SimpleStrategy', 'replication_factor' : 3};"))
							cc.withSessionDo ( session => session.execute(
									"USE dk_test_keyspace"))
							cc.withSessionDo ( session => session.execute(
									"CREATE TABLE IF NOT EXISTS dk_test_tweetstream (tweet_text text  PRIMARY KEY,tweet_createdat text);"))

							val ssc = new StreamingContext(sc, Seconds(5))

							val flumeStream = FlumeUtils.createPollingStream(ssc, "localhost" , 9999)

							println("231232132131")
							val lines = flumeStream.map ( x => {
								//val tweetcontent = 
								JSON.parseFull(new String(x.event.getBody.array())).getOrElse(0).asInstanceOf[Map[String,String]]
							})
							.filter(x => {
								x("lang") == "en"
							})
							.map(x=>{
								//println(x.keys)
								//(detectSentiment(x("text")),x("text"), x("created_at")) //fixme the const
								(x("text"), x("created_at"))
							}
									)
							//https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object
							//Set(coordinates, retweeted, source, entities, reply_count, favorite_count, in_reply_to_status_id_str, geo, id_str, in_reply_to_user_id, timestamp_ms, truncated, text, retweet_count, retweeted_status, id, in_reply_to_status_id, filter_level, created_at, place, favorited, lang, contributors, in_reply_to_screen_name, is_quote_status, in_reply_to_user_id_str, user, quote_count)
							//println(tweetcontent.keys)


							// dk: now have format 
							//(tweetcontent("text"),tweetcontent("created_at"))
							// for full list (tweetcontent("text"), tweetcontent("created_at"), tweetcontent("source"), tweetcontent("user"),tweetcontent("place"))

							//TODO!!!: filter english - using lang attr    




							lines.print()

							lines.foreachRDD(rdd => {
								// TODO: SETUP CASS KEYSPACE AND TABLE FIRST!!!!!!!!!!!!!!!!!!!!!!!!!!!!! then do prediction..........
								rdd.saveToCassandra("dk_test_keyspace","dk_test_tweetstream") //SomeColumns("tweet_text", "tweet_createdat")
							})



							ssc.start()
							ssc.awaitTermination()
	}



	def circleArea (r: Double) : Double = {
			return math.pow(r,2)*PI
	}

	def circleAreaStr (r: String) : Double = {
			// need to worry about empty string

			r.isEmpty() match {
			case true => 0
			case false => math.pow(r.toDouble,2)*PI
			}

	}

	def pdtArrayElem (intArr: List[Int]): Int = {
			@tailrec
			def tailrecur (intArr: List[Int], intAccum : Int): Int = {
					//println(intArr)
					intArr match {
					case Nil => {
						intAccum // Nil refers to the empty list

					}
					case x :: tail => { // if not empty list, use cons op. x::y means x is first elem and y is remaining elems
						//println (tail) // should print remaining elems
						tailrecur (tail, x * intAccum) 
					}
					}  
			}

			tailrecur(intArr, 1) // 1 is the base value to start the recursion
	}

	def exponentfx (base :Int, exponent: Int) = {
			@tailrec
			def tailrecur (base: Int, exponent: Int, accum:Int) : Int = { //MUST HAVE RETURN TYPE... also = sign after def (assigning lambda to named def?).
					// e.g. 2^10 is actually 2*2*2.. 10 (exponent) times..
					exponent match {
					case x if x<1 => {
						accum
					}
					case _ => {
						//print("accum" + accum)
						//print("expon" +  exponent)
						tailrecur (base, exponent -1, accum * base)
					}
					}
			}

			tailrecur(base, exponent, 1)
	}



	def testSpark (args: Array[String]) {
		//Create conf object
		val conf = new SparkConf().setAppName("WordCount").setMaster("local[2]") // run locally with 2 cores

				//create spark context object
				val sc = new SparkContext(conf)

				//Check whether sufficient params are supplied
				if (args.length < 2) {
					println("Usage: ScalaWordCount <input> <output>")
					System.exit(1)
				}
		//Read file and create RDD
		val rawData = sc.textFile(args(0))
				//convert the lines into words using flatMap operation
				val words = rawData.flatMap(line => line.split(" "))
				//count the individual words using map and reduceByKey operation
				val wordCount = words.map(word => (word, 1)).reduceByKey(_ + _)
				//Save the result
				wordCount.saveAsTextFile(args(1))
				//stop the spark context
				sc.stop
	}


	def tuplediff (src:(Int, Int), dest: (Int, Int)) : (Int,Int) = {
			// to compute offset take dist - src
			(dest._1 - src._1, dest._2 - src._2) 
	}


}